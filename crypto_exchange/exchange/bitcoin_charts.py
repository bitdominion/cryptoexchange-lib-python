import requests

from datetime import datetime
from decimal import Decimal
from hashlib import sha256

from crypto_exchange.order import BuyOrder, SellOrder
from crypto_exchange.trade import Trade
from crypto_exchange.transport.multi_endpoint import MultiEndpointCSVAPI
from crypto_exchange.exchange.public_exchange import PublicExchange


def parse_timestamp(milliseconds):
    dt = datetime.fromtimestamp(int(milliseconds))
    return dt


class BitcoinCharts(PublicExchange):
    
    BASE_URL = "http://api.bitcoincharts.com/v1/"

    def __init__(self):
        self.api = MultiEndpointCSVAPI(self.BASE_URL)

    def get_raw_tradebook(self, market, start_at):
        assert start_at, "`start_at` is required for BitcoinCharts"
        
        tradebook = self.api.perform_request(
            "trades.csv", 
            {"symbol": market[2], "start": start_at})
        tradebook = list(tradebook)

        for trade in tradebook:
            trade[0] = parse_timestamp(trade[0])
            trade[1] = Decimal(trade[1])
            trade[2] = Decimal(trade[2])

        return tradebook
    
    def cook_trade(self, market, raw_trade):
        trade = Trade(
            raw_trade[0],                    # trade ID
            market[0],          # base currency
            market[1],        # counter currency
            raw_trade[0],
            None,                               # order ID (we don't get this?)
            raw_trade[2],
            raw_trade[1],
            raw_data=raw_trade
        )
        
        return trade

    def get_raw_orderbook(self, market):
        raise NotImplementedError
    
    def cook_order(self, market, order_type, raw_order):
        raise NotImplementedError

    def get_orderbook(self, market):
        raise NotImplementedError
    
    # def get_markets(self):
    #     all_currencies = self.api.perform_request("public/currency")
    #     virtual_currencies = [x for x in all_currencies if x["virtual"]]
    #     fiat_currencies = [x for x in all_currencies if not x["virtual"]]

    #     return [
    #         (y["code"], x["code"])
    #         for x, y in product(virtual_currencies, fiat_currencies)
    #    ]
    
    # def get_ticker(self, markets=None, **kw):
    #     if not markets:
    #         markets = [("BTC", "CAD")]
            
    #     out_dicts = {"time": datetime.now()}
        
    #     for market in markets:
    #         r = requests.get(self.BASE_URL + "public/ticker", params={"order_currency": market[0], "payment_currency": market[1]})
    #         data = r.json()
            
    #         if data["status"] == "success":
    #             out_dict = {
    #                 "volume": Decimal(data["data"]["volume_1day"]["value"]),
    #                 "buy": Decimal(data["data"]["closing_price"]["value"]),
    #                 "sell": Decimal(data["data"]["closing_price"]["value"])
    #             }
    #             out_dicts[(market[0], market[1])] = out_dict
                
    #     return out_dicts