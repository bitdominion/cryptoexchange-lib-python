import requests

from datetime import datetime
from decimal import Decimal
from hashlib import sha256

from crypto_exchange.order import BuyOrder, SellOrder
from crypto_exchange.trade import Trade
from crypto_exchange.transport.multi_endpoint import MultiEndpointJSONAPI
from crypto_exchange.exchange.public_exchange import PublicExchange


def parse_timestamp(milliseconds):
    dt = datetime.fromtimestamp(milliseconds)
    return dt


class CaVirtexPublic(PublicExchange):
    
    BASE_URL = "https://cavirtex.com/api2/"

    def __init__(self):
        self.api = MultiEndpointJSONAPI(self.BASE_URL)

    def get_raw_tradebook(self, market):
        tradebook = self.api.perform_request("trades.json", 
            {"currencypair": market[0].upper() + market[1].upper()})

        for trade in tradebook:
            trade["date"] = parse_timestamp(trade["date"])

        return tradebook
    
    def cook_trade(self, market, raw_trade):
        trade = Trade(
            raw_trade["id"],                    # trade ID
            raw_trade["for_currency"],          # base currency
            raw_trade["trade_currency"],        # counter currency
            raw_trade["date"],
            None,                               # order ID (we don't get this?)
            raw_trade["amount"],
            raw_trade["price"],
            raw_data=raw_trade
        )
        
        return trade

    def get_raw_orderbook(self, market):
        orderbook = self.api.perform_request("orderbook.json", 
            {"currencypair": market[0].upper() + market[1].upper()})

        formatted_bids = []
        formatted_asks = []

        for order in orderbook["bids"]:
            formatted_bids.append([Decimal(order[0]), Decimal(order[1])])

        for order in orderbook["asks"]:
            formatted_asks.append([Decimal(order[0]), Decimal(order[1])])

        orderbook["bids"] = formatted_bids
        orderbook["asks"] = formatted_asks

        return orderbook
    
    def cook_order(self, market, order_type, raw_order):
        order_class = BuyOrder if order_type == "buy" else SellOrder
        order_id = sha256(repr(raw_order)).hexdigest()
        order = order_class(
            order_id,               # order ID, we don't get this so make it up
            market[0],              # base currency
            market[1],              # counter currency
            None,                   # date/time, we don't get this
            raw_order[1],    # amount
            raw_order[0],      # price
            raw_data=raw_order
        )
        
        return order
    
    # def get_markets(self):
    #     all_currencies = self.api.perform_request("public/currency")
    #     virtual_currencies = [x for x in all_currencies if x["virtual"]]
    #     fiat_currencies = [x for x in all_currencies if not x["virtual"]]

    #     return [
    #         (y["code"], x["code"])
    #         for x, y in product(virtual_currencies, fiat_currencies)
    #    ]
    
    # def get_ticker(self, markets=None, **kw):
    #     if not markets:
    #         markets = [("BTC", "CAD")]
            
    #     out_dicts = {"time": datetime.now()}
        
    #     for market in markets:
    #         r = requests.get(self.BASE_URL + "public/ticker", params={"order_currency": market[0], "payment_currency": market[1]})
    #         data = r.json()
            
    #         if data["status"] == "success":
    #             out_dict = {
    #                 "volume": Decimal(data["data"]["volume_1day"]["value"]),
    #                 "buy": Decimal(data["data"]["closing_price"]["value"]),
    #                 "sell": Decimal(data["data"]["closing_price"]["value"])
    #             }
    #             out_dicts[(market[0], market[1])] = out_dict
                
    #     return out_dicts