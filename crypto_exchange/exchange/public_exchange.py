class PublicExchange(object):
    """
    Encapsulates common methods exposed by an exchange which does not require 
    authentication.
    """
    
    def get_raw_tradebook(self, market, start_at=None):
        """
        Calls the exchange's API and returns a list of trades. It may be in 
        whatever format the exchange has provided to us. At a minimum, try to
        do any necessary data conversions.
        """
        raise NotImplementedError
    
    def cook_trade(self, market, raw_trade):
        """
        Convert the provided `raw_trade` from the exchange and return a
        `crypto_exchange.trade.Trade` object.
        """
        raise NotImplementedError
    
    def get_tradebook(self, market, start_at=None):
        """
        Calls `get_raw_tradebook` and returns a list of 
        `crypto_exchange.trade.Trade` objects.
        
        :param market: a tuple representing a currency pair ie: ("BTC", "CAD")
        """
        
        trade_objs = []
        trade_data = self.get_raw_tradebook(market, start_at)

        for trade in trade_data:
            trade_objs.append(self.cook_trade(market, trade))

        return trade_objs
    
    def get_raw_orderbook(self, market):
        """
        Calls the exchange's API and returns a list of buy and sell orders. It 
        may be in whatever format the exchange has provided to us. At a 
        minimum, try to do any necessary data conversions.
        """
        raise NotImplementedError
    
    def cook_order(self, market, raw_order):
        """
        Convert the provided `raw_order` from the exchange and return a
        `crypto_exchange.order.BuyOrder` or `SellOrder` object.
        """
        raise NotImplementedError

    def get_orderbook(self, market):
        """
        Calls `get_raw_orderbook` and returns a 2-tuple of list of 
        `crypto_exchange.order.BuyOrder` objects and a list of
        `crypto_exchange.order.SellOrder` objects.
        
        :param market: a tuple representing a currency pair ie: ("BTC", "CAD")
        """
        
        bids = []
        asks = []
        order_data = self.get_raw_orderbook(market)

        for order in order_data["bids"]:
            bids.append(self.cook_order(market, "buy", order))

        for order in order_data["asks"]:
            asks.append(self.cook_order(market, "sell", order))

        return bids, asks
    
    def get_ticker(self, markets, **kw):
        """
        Information about bidding on a pair, such as: the highest price, lowest
        price, average price, trading volume, trading volume in the currency of
        the last deal, the price of buying and selling.
        
        Args:
        
        :param markets:		List of tuple of (BTC, CAD) to return ticker data for.
        	If None return data for all markets (if supported by exchange).
            
        Returns:
        
        Dictionary with structure:
        
        {
        	("BTC", "CAD"): {
                "volume": Decimal,
                "buy": Decimal,
                "sell": Decimal
            }
        }

        Depending on the exchange, information may be live or delayed by some
        amount of time.
        """
        raise NotImplementedError
