
import requests

from datetime import datetime
from decimal import Decimal
from hashlib import sha256

from crypto_exchange.order import BuyOrder, SellOrder
from crypto_exchange.trade import Trade
from crypto_exchange.transport.multi_endpoint import MultiEndpointJSONAPI
from crypto_exchange.exchange.public_exchange import PublicExchange


def parse_timestamp(milliseconds):
    timestamp = int(milliseconds) / 1000.0
    dt = datetime.fromtimestamp(timestamp)
    return dt


class QuadrigaCXPublic(PublicExchange):
    
    BASE_URL = "https://api.quadrigacx.com/"

    def __init__(self):
        self.api = MultiEndpointJSONAPI(self.BASE_URL)

    def get_raw_tradebook(self, market, start_at=None):
        tradebook = self.api.perform_request("public/trades", {"book": 
            market[0].lower() + "_" + market[1].lower()})

        for trade in tradebook:
            trade["datetime"] = parse_timestamp(trade["datetime"])
            trade["amount"] = Decimal(trade["amount"])
            trade["rate"] = Decimal(trade["rate"])
            trade["value"] = Decimal(trade["value"])
            del trade["date"]
            
        return tradebook
    
    def cook_trade(self, market, raw_trade):
        trade_id = sha256(repr(raw_trade)).hexdigest()
        trade = Trade(
            trade_id,               # trade ID
            market[0],              # base currency
            market[1],              # counter currency
            raw_trade["datetime"],
            None,                   # order ID (we don't get this?)
            raw_trade["amount"],
            raw_trade["rate"],
            raw_data=raw_trade
        )
        
        return trade

    def get_raw_orderbook(self, market):
        orderbook = self.api.perform_request("public/orders", {"book": 
            market[0].lower() + "_" + market[1].lower()})

        for order in orderbook["buy"]:
            order["amount"] = Decimal(order["amount"])
            order["value"] = Decimal(order["value"])
            order["rate"] = Decimal(order["rate"])

        for order in orderbook["sell"]:
            order["amount"] = Decimal(order["amount"])
            order["value"] = Decimal(order["value"])
            order["rate"] = Decimal(order["rate"])

        return orderbook
    
    def cook_order(self, market, order_type, raw_order):
        order_class = BuyOrder if order_type == "buy" else SellOrder
        order_id = sha256(repr(raw_order)).hexdigest()
        order = order_class(
            order_id,               # order ID, we don't get this so make it up
            market[0],              # base currency
            market[1],              # counter currency
            None,                   # date/time, we don't get this
            raw_order["amount"],    # amount
            raw_order["rate"],      # price
            raw_data=raw_order
        )
        
        return order
    
    # def get_ticker(self, markets=None, **kw):
    #     r = requests.get(self.BASE_URL + "public/info")
    #     data = r.json()
    #     out_dict = {}
    
    #     for str_pair in data.keys():
    #         currency_pair = tuple(str_pair.split("_"))
            
    #         pair_data = data[str_pair]
    #         out_dict[currency_pair] = {x: Decimal(pair_data[x]) for x in pair_data.keys() if x != "rate"}
    #         out_dict["time"] = datetime.now()
            
    #     return out_dict