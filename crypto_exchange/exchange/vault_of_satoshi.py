
import requests

from datetime import datetime
from decimal import Decimal
from itertools import product
from hashlib import sha256

from crypto_exchange.order import BuyOrder, SellOrder
from crypto_exchange.trade import Trade
from crypto_exchange.transport.multi_endpoint import MultiEndpointJSONAPI
from crypto_exchange.exchange.public_exchange import PublicExchange


def parse_timestamp(microseconds):
    milliseconds = microseconds / 1000 / 1000
    dt = datetime.fromtimestamp(milliseconds)
    return dt


class VaultOfSatoshiPublic(PublicExchange):
    
    BASE_URL = "https://api.vaultofsatoshi.com/"

    def __init__(self):
        self.api = MultiEndpointJSONAPI(self.BASE_URL)

    def get_raw_tradebook(self, market, start_at=None):
        tradebook = self.api.perform_request("public/recent_transactions", 
            {"order_currency": market[0].upper(), 
             "payment_currency": market[1].upper()})

        for trade in tradebook:
            trade["transaction_date"] = parse_timestamp(trade["transaction_date"])
            trade["inverse_trade"] = bool(trade["inverse_trade"])
            trade["price"] = Decimal(trade["price"]["value"])
            trade["total"] = Decimal(trade["total"]["value"])
            trade["units_traded"] = Decimal(trade["units_traded"]["value"])
            trade["transaction_id"] = int(trade["transaction_id"])
            trade["journal_id"] = int(trade["journal_id"])

        return tradebook
    
    def cook_trade(self, market, raw_trade):
        trade = Trade(
            raw_trade["transaction_id"],    # trade ID
            market[0],                      # base currency
            market[1],                      # counter currency
            raw_trade["transaction_date"],
            None,                           # order ID (we don't get this?)
            raw_trade["units_traded"],
            raw_trade["price"],
            raw_data=raw_trade
        )
        
        return trade

    def get_raw_orderbook(self, market):
        orderbook = self.api.perform_request("public/orderbook", 
            {"order_currency": market[0].upper(), 
             "payment_currency": market[1].upper(),
             "group_orders": "0"})

        orderbook["timestamp"] = parse_timestamp(orderbook["timestamp"])

        for order in orderbook["bids"]:
            order["price"] = Decimal(order["price"]["value"])
            order["total"] = Decimal(order["total"]["value"])
            order["quantity"] = Decimal(order["quantity"]["value"])

        for order in orderbook["asks"]:
            order["price"] = Decimal(order["price"]["value"])
            order["total"] = Decimal(order["total"]["value"])
            order["quantity"] = Decimal(order["quantity"]["value"])

        return orderbook
    
    def cook_order(self, market, order_type, raw_order):
        order_class = BuyOrder if order_type == "buy" else SellOrder
        order_id = sha256(repr(raw_order)).hexdigest()
        order = order_class(
            order_id,               # order ID, we don't get this so make it up
            market[0],              # base currency
            market[1],              # counter currency
            None,                   # date/time, we don't get this
            raw_order["quantity"],  # amount
            raw_order["price"],     # price
            raw_data=raw_order
        )
        
        return order

    def get_markets(self):
        all_currencies = self.api.perform_request("public/currency")
        virtual_currencies = [x for x in all_currencies if x["virtual"]]
        fiat_currencies = [x for x in all_currencies if not x["virtual"]]

        return [
            (y["code"], x["code"])
            for x, y in product(virtual_currencies, fiat_currencies)
        ]
    
    # def get_ticker(self, markets=None, **kw):
    #     if not markets:
    #         markets = [("BTC", "CAD")]
            
    #     out_dicts = {"time": datetime.now()}
        
    #     for market in markets:
    #         r = requests.get(self.BASE_URL + "public/ticker", params={"order_currency": market[0], "payment_currency": market[1]})
    #         data = r.json()
            
    #         if data["status"] == "success":
    #             out_dict = {
    #                 "volume": Decimal(data["data"]["volume_1day"]["value"]),
    #                 "buy": Decimal(data["data"]["closing_price"]["value"]),
    #                 "sell": Decimal(data["data"]["closing_price"]["value"])
    #             }
    #             out_dicts[(market[0], market[1])] = out_dict
                
    #     return out_dicts