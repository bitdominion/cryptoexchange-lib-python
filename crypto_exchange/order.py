class Order(object):
    """
    Basic order
    """
    order_type = 0
    def __init__(self, order_id, base_currency, counter_currency, datetime, 
                 amount, price, raw_data=None):
        """
        :param order_id: id of referencing order on the exchange
        :param base_currency: the base currency of the trade (LTC in "LTC/BTC")
        :param counter_currency: the counter currency of the trade (BTC in "LTC/BTC")
        :param datetime: date and time of the trade in UTC
        :param amount: amount that was bought/sold
        :param price: price at which the trade was made
        :param raw_data: data to annotate the trade with
        """

        self.order_id = order_id
        self.base_currency = base_currency
        self.counter_currency = counter_currency
        self.datetime = datetime
        self.amount = amount
        self.price = price
        self.raw_data = raw_data

    def type(self):
        return self.__class__.__name__

    def __str__(self):
        return "{0} on {1} for {2} {3} at {4} {5}".format(
            self.__class__, self.datetime,
            self.amount, self.base_currency,
            self.price, self.counter_currency)

class BuyOrder(Order):
    order_type = 1

class SellOrder(Order):
    order_type = 2
