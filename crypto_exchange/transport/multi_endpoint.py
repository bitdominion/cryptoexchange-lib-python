import csv
import requests

from urlparse import urljoin
from decimal import Decimal

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

from crypto_exchange.exception import APIException


class MultiEndpointAPI(object):
    """
    Used for Vault of Satoshi, Quadriga CX and CAVirtex exchanges.
    """

    def __init__(self, base_url, key=None, secret=None):
        self.base_url = base_url
        self.authenticated = key and secret
        self.key = key
        self.secret = secret

    def get_request_params(self, method, data):
        return data, {"User-Agent": "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36"}

    def perform_request(self, method, data={}):
        payload, headers = self.get_request_params(method, data)
        endpoint_url = urljoin(self.base_url, method)

        if self.authenticated:
            r = requests.post(endpoint_url, data=payload, headers=headers)
        else:
            r = requests.get(endpoint_url, params=payload, headers=headers)
            
        return r
            
            
class MultiEndpointJSONAPI(MultiEndpointAPI):
    
    def perform_request(self, *a, **kw):
        response = super(MultiEndpointJSONAPI, self).perform_request(*a, **kw)

        content = response.json(parse_float=Decimal)

        # CAVirtex does not usually return a dict with a status field. If
        # we have a list, assume everything is cool-io.

        if isinstance(content, list):
            return content

        if content.has_key("status") and content["status"] not in ("success", "ok"):
            raise APIException(content.get("message"))

        if content.has_key("data"):
            return content["data"]
        elif content.has_key("trades"):
            return content["trades"]
        elif content.has_key("orderbook"):
            return content["orderbook"]
        
        
class MultiEndpointCSVAPI(MultiEndpointAPI):
    
    def perform_request(self, *a, **kw):
        response = super(MultiEndpointCSVAPI, self).perform_request(*a, **kw)
        csv_reader = csv.reader(response.iter_lines())
        return csv_reader