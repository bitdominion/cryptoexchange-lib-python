from distutils.core import setup
from crypto_exchange import __version__

setup(
    name="crypto_exchange",
    version=__version__,
    author="Arthur Lucas",
    author_email="artlucas@gmail.com",
    packages=["crypto_exchange", "crypto_exchange.test", "crypto_exchange.exchange"],
    install_requires=[
        "requests>=2.1.0,<3.0",
    ],
    tests_require=[
        'httpretty>=0.8.0',
        'pytz>=2013.8',
    ]
)
